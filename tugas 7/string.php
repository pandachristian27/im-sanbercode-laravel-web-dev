<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Document</title>
</head>
<body>
    <h2>Contoh Soal</h2>
    <?php 

    echo "<h3>Soal No 1</h3>";

    // //SOAL NO 1
    // Tunjukan dengan menggunakan echo berapa panjang dari string yang diberikan berikut! Tunjukkan juga jumlah kata di dalam kalimat tersebut! 
    $kalimat1 = "PHP is never old";
    echo "kalimat pertama : ".$kalimat1 . "<br>";
    echo "Jumlah karakter kalimat pertama : ". strlen($kalimat1). "<br>";
    echo "Jumlah kata : ". str_word_count($kalimat1)."<br><br>";

    // $first_sentence = "Hello PHP!" ; // Panjang string 10, jumlah kata: 2
    // $second_sentence = "I'm ready for the challenges"; // Panjang string: 28,  jumlah kata: 5


    echo "<h3>Contoh 2</h3>";

    // SOAL NO 2
    // Mengambil kata pada string dan karakter-karakter yang ada di dalamnya. 
    $string2 = "I love PHP";
    echo "Kalimat Kedua : " .$string2."<br>";
    echo "Kata pertama : ". substr($string2, 0, 1). "<br>";
    echo "Kata kedua : ". substr($string2, 2, 4). "<br>";
    echo "Kata ketiga : ". substr($string2, 7, 3). "<br><br>";

    // $string2 = "I love PHP";
        
    //     echo "<label>String: </label> \"$string2\" <br>";
    //     echo "Kata pertama: " . substr($string2, 0, 1) . "<br>" ; 
    //     // Lanjutkan di bawah ini
    //     echo "Kata kedua: " ;
    //     echo "<br> Kata Ketiga: " ;


    echo "<h3>Contoh 3</h3>";

    // SOAL NO 3
    //         Mengubah karakter atau kata yang ada di dalam sebuah string.
    $string3 = "PHP is old but sexy!";
    echo "Kalimat ketiga : ". $string3. "<br>";
    echo "Ganti kalimat keempat : ". str_replace("sexy","awesome",$string3);

    ?>

</body>
</html>