<?php

require_once 'Animal.php';

class Ape extends Animal {
    public function yell() {
        echo "Jump : Auooo" . PHP_EOL;
    }

    public function __construct($name) {
        parent::__construct($name);
        $this->legs = 2;
    }
}

?>
